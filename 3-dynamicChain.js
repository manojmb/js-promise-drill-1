function dynamicChain(functions) {
    return new Promise((resolve, reject) => {
        let chain = Promise.resolve();

        for (const func of functions) {
            chain = chain.then(result => func(result));
        }

        chain
            .then((finalResult) => {
                resolve(finalResult);
            })
            .catch((error) => {
                reject(error);
            });
    });
}


const function1 = () => new Promise(resolve => setTimeout(() => resolve("Result 1"), 1000));
const function2 = (result) => new Promise(resolve => setTimeout(() => resolve(`${result} + Result 2`), 500));
const function3 = (result) => new Promise(resolve => setTimeout(() => resolve(`${result} + Result 3`), 1500));

const functionsArray = [function1, function2, function3];

dynamicChain(functionsArray)
    .then(finalResult => {
        console.log("Final Result:", finalResult);
    })
    .catch(error => {
        console.error("At least one Promise rejected:", error);
    });
