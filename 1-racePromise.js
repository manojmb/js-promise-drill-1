function getRandomDelay() {
    return Math.floor(Math.random() * (2001) + 1000);
}

function racePromise1() {
    return new Promise((resolve) => {
        const delay = getRandomDelay();
        setTimeout(() => {
            resolve(`Race Promise 1 resolved after ${delay} milliseconds`);
        }, delay);
    });
}

function racePromise2() {
    return new Promise((resolve) => {
        const delay = getRandomDelay();
        setTimeout(() => {
            resolve(`Race Promise 2 resolved after ${delay} milliseconds`);
        }, delay);
    });
}

function racePromises() {
    return Promise.race([racePromise1(), racePromise2()]);
}



racePromises()
    .then((winner) => {
        console.log(winner);
    })
    .catch((error) => {
        console.error("At least one promise rejected:", error);
    });
