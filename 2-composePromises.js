function composePromises(promises) {
    return new Promise((resolve, reject) => {
        const results = [];
        let completedPromises = 0;

        promises.forEach((promise, index) => {
            promise
                .then((result) => {
                    results[index] = result;
                    completedPromises++;
                    if (completedPromises === promises.length) {
                        resolve(results);
                    }
                })
                .catch(error => {
                    reject(error);
                });
        });
    });
}

const promise1 = new Promise((resolve) => setTimeout(() => resolve("Result 1"), 1000));
const promise2 = new Promise((resolve) => setTimeout(() => resolve("Result 2"), 500));
const promise3 = new Promise((resolve) => setTimeout(() => resolve("Result 3"), 1500));

const composedPromise = composePromises([promise1, promise2, promise3]);
try {
    composedPromise
        .then(results => {
            console.log("All promises resolved:", results);
        })
        .catch(error => {
            console.error("At least one promise rejected:", error);
        });
} catch (e) {
    console.error(e.message);
}