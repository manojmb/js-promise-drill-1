function parallelLimit(promises, limit) {
    return new Promise((resolve, reject) => {
        if (!Array.isArray(promises) || !Number.isInteger(limit) || limit <= 0) {
            reject(new Error('Invalid input. Promises must be an array, and limit must be a positive integer.'));
            return;
        }

        const results = new Array(promises.length).fill(undefined);
        let currentIndex = 0;

        function runNextPromise() {
            if (currentIndex >= promises.length) {
                // Check if all promises have completed
                if (results.every(result => result !== undefined)) {
                    resolve(results);
                }
                return;
            }

            const currentPromiseIndex = currentIndex;
            currentIndex++;

            const currentPromise = promises[currentPromiseIndex];

            currentPromise()
                .then(result => {
                    results[currentPromiseIndex] = result;
                    runNextPromise();
                })
                .catch(error => {
                    reject(error);
                });
        }

        for (let i = 0; i < Math.min(limit, promises.length); i++) {
            runNextPromise();
        }
    });
}

const asyncFunction = (id) => () => new Promise(resolve => setTimeout(() => resolve(`Result ${id}`), 1000));

const promisesArray = [
    asyncFunction(1),
    asyncFunction(2),
    asyncFunction(3),
    asyncFunction(4),
    asyncFunction(5),
];

const limit = 5;

parallelLimit(promisesArray, limit)
    .then(results => {
        console.log("All promises resolved:", results);
    })
    .catch(error => {
        console.error("At least one promise rejected:", error);
    });
